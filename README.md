# cesar
Cesar (de)ciphers a given text with Caesar Cipher.

## Installation
- Via pip:  
`sudo pip install cesar`
- Or download the source code and use at will.


## Calling cesar
If installed via pip it should be in your PATH, so simply use it with:  
`cesar [options]`

As a Python script, it can be called (from its own folder) with:  
`python cesar [options]`

Or if made the file executable, it can be called (from its own folder) with:  
`./cesar [options]`

## Usage
Cesar is written so it behaves as any other GNU/Linux console command.

Admits the input message in three different ways:
- Direct input after calling the command (default).
- Add input to the command (-m option).
- Read input from file (-i option).

The input can either be:
- A plain message to cipher (default).
- A ciphered message to decipher (-d option).

The ouput will be either:
- Shown in the console (default).
- Written to a file (-o option, overwrites, add -a to append).

Help can be seen using -h, --help option.

Version can be seen using --version option.

## Notes
Due to the cyclical nature of this cipher, there is no actual difference between
encryption and decryption but the key used.

For example, a shift of 3 to the left is equivalent to a shift of 23 to the right.

## Warning
This cipher is not secure AT ALL. It is provided for its historical significance (and also for fun!).

DO NOT USE IT TO CIPHER ANY INFORMATION YOU WOULD LIKE TO REMAIN SECURE.
